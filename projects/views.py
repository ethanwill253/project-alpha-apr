from projects.models import Project
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse, reverse_lazy


class ProjectListView(LoginRequiredMixin, ListView):
    model = Project
    template_name = "projects/list.html"

    def get_queryset(self):
        user = self.request.user
        return Project.objects.filter(members=user)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if (self.request.user.is_authenticated):
            num_complete = []
            for pro in context['project_list']:
                num_complete_section = []
                count = 0
                for j in pro.tasks.values():
                    task_complete = j['is_completed']
                    if task_complete is True:
                        num_complete_section.append(task_complete)
                        count += 1
                if len(num_complete_section) == count:
                    num_complete.append(num_complete_section)
            context['is_complete'] = num_complete
            return context


class ProjectDetailView(LoginRequiredMixin, DetailView):
    model = Project
    template_name = "projects/detail.html"
    fields = ["name", "description"]

    def get_queryset(self):
        user = self.request.user
        return Project.objects.filter(members=user)


class ProjectCreateView(LoginRequiredMixin, CreateView):
    model = Project
    template_name = "projects/create.html"
    fields = ["name", "description", "members", "urgent"]

    def get_success_url(self):
        return reverse("show_project", args=[self.object.id])


class ProjectDeleteView(LoginRequiredMixin, DeleteView):
    model = Project
    template_name = "projects/delete.html"
    success_url = reverse_lazy('list_projects')


class ProjectUpdateView(LoginRequiredMixin, UpdateView):
    model = Project
    template_name = "projects/edit.html"
    fields = ["name", "description", "members", "urgent", "archive"]

    def get_success_url(self):
        return reverse("show_project", args=[self.object.id])
