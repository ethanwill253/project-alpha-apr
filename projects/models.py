from django.db import models
from django.conf import settings
# from tasks.models import Task


USER_MODEL = settings.AUTH_USER_MODEL


class Project(models.Model):
    name = models.CharField(max_length=200)
    description = models.TextField()
    urgent = models.BooleanField(default=False)
    archive = models.BooleanField(default=False)
    members = models.ManyToManyField(
        USER_MODEL,
        related_name="projects",
    )

    class Meta:
        ordering = ['archive', '-urgent']

    def __str__(self):
        return self.name
